class Snake:
    def __init__(self, x, y, dx, dy, speed, length, fragments, thickness):
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy
        self.speed = speed
        self.length = length
        self.fragments = fragments
        self.thickness = thickness

    def grow(self):
        print("Grow")
        self.length += 1

    def crawl(self):
        self.x += self.dx
        self.y += self.dy

    def turn_left(self):
        print("Turn left")
        self.dx = -self.thickness
        self.dy = 0

    def turn_right(self):
        print("Turn right")
        self.dx = self.thickness
        self.dy = 0

    def climb_up(self):
        print("Climb up")
        self.dx = 0
        self.dy = -self.thickness

    def climb_down(self):
        print("Climb down")
        self.dx = 0
        self.dy = self.thickness

    def bump(self, screen_width, screen_height):
        if self.x >= screen_width or self.x < 0 or self.y >= screen_height or self.y < 0:
            return True
        else:
            return False

    def eat(self, fx, fy):
        if self.x == fx and self.y == fy:
            return True
        else:
            return False
