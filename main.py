import pygame
import time
import random

from snake import Snake
from food import Food

pygame.init()

# colors
WHITE = (255,255,255)
BLACK = (0,0,0)

# screen
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('Snake')

# game
clock = pygame.time.Clock()
font = pygame.font.SysFont(None, 25)

def score(score):
    value = font.render(str(score), True, WHITE)
    screen.blit(value, [10, 10])

def message(msg):
    text = font.render(msg, True, WHITE)
    text_rect = text.get_rect(center=(SCREEN_WIDTH/2, SCREEN_HEIGHT/2))
    screen.blit(text, text_rect)

def main():

    is_game_over = False

    snake = Snake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 0, 0, 30, 1, [], 10)
    pygame.draw.rect(screen, WHITE, [snake.x, snake.y, snake.thickness, snake.thickness])
    pygame.display.update()

    food = Food(round(random.randrange(0, SCREEN_WIDTH - snake.thickness) / 10.0) * 10.0, round(random.randrange(0, SCREEN_HEIGHT - snake.thickness) / 10.0) * 10.0, snake.thickness)

    while not is_game_over:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_game_over = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    snake.turn_left()
                elif event.key == pygame.K_RIGHT:
                    snake.turn_right()
                elif event.key == pygame.K_UP:
                    snake.climb_up()
                elif event.key == pygame.K_DOWN:
                    snake.climb_down()

        if snake.bump(SCREEN_WIDTH, SCREEN_HEIGHT):
            is_game_over = True

        snake.crawl()
        screen.fill(BLACK)
        pygame.draw.rect(screen, WHITE, [food.x, food.y, food.thickness, food.thickness])

        snake_fragment = []
        snake_fragment.append(snake.x)
        snake_fragment.append(snake.y)
        snake.fragments.append(snake_fragment)

        if len(snake.fragments) > snake.length:
            del snake.fragments[0]

        for x in snake.fragments[:-1]:
            if x == snake_fragment:
                is_game_over = True

        for x in snake.fragments:
            pygame.draw.rect(screen, WHITE, [x[0], x[1], snake.thickness, snake.thickness])

        score(snake.length - 1)
        pygame.display.update()

        if snake.eat(food.x, food.y):
            food = Food(round(random.randrange(0, SCREEN_WIDTH - snake.thickness) / 10.0) * 10.0, round(random.randrange(0, SCREEN_HEIGHT - snake.thickness) / 10.0) * 10.0, snake.thickness)
            snake.grow()

        clock.tick(snake.speed)

    screen.fill(BLACK)
    message("GAME OVER")
    pygame.display.update()
    time.sleep(3)

    pygame.quit()
    quit()

main()
